import { LitElement,html,css } from 'lit-element';

class ExamenComponent extends LitElement{
    static get properties(){
        return {
            title: String,
            subTitle: String,
            resultado: Number,
            valores: Array
        }
    }
    static get styles() {
        return [css`
          .res{
            background-color: var(--background-color,#333);          
            color:white
          }

        `];
      }
    constructor(){
        super();
        this.title = 'Titulo';
        this.subTitle = 'Subtitulo';
        this.resultado = 0;
        this.valores = [4,16,36];
    }
    sumaDeDosNumeros(){
      const [val1,val2] = this.shadowRoot.querySelectorAll('input');
      this.resultado = parseFloat(val1.value) + parseFloat(val2.value);
    }
    calculaRaiz(){
        var raices = this.valores.map(Math.sqrt);
        console.log(raices);
        return raices;
    }
    render(){
        return html `
        
         <h2>Función suma de dos numeros </h2>
         <input name="val1"></input>
         <input name="val2"></input>

         <button @click="${this.sumaDeDosNumeros}"> Suma </button>
         <h1 class="res">Resultado: ${this.resultado}<h1>
         <button @click="${this.calculaRaiz}"> Raiz Cuadrada de Map </button>
        `;
    }
}


customElements.define('examen-component', ExamenComponent);
